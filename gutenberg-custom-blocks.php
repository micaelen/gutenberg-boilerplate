<?php 
/**
 * Plugin Name:       Gutenberg - Custom Blocks
 * Description:       A collection of custom Gutenberg blocks.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Micaelen Miranda     
 * License:           GPL v2 or later
 */

require_once dirname( __FILE__ ) . '/inc/categories.php';

function cg_register_block() {
    wp_register_script(
        'blocks-js',
        plugins_url( '/dist/js/blocks.min.js', __FILE__ ),
        [
            'wp-element',
            'wp-editor',
            'wp-i18n',
            'wp-blocks',
        ],
        '',
        NULL,
        TRUE
    );
    
    wp_register_style(
        'blocks-css',
        plugins_url( '/dist/css/index.min.css', __FILE__ ),
        [],
        NULL
    );
    
    register_block_type(
        'example/block', [
            'editor_script' => 'blocks-js',
            'editor_style'  => 'blocks-css',
            'style'         => 'blocks-css',
        ]
    );
}
add_action( 'init', 'cg_register_block' );
