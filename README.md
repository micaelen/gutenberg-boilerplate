## Gutenberg Custom Blocks - Boilerplate

This is a plugin structure for development of custom Gutenberg blocks in WordPress.

### Technologies

- [Yarn](https://yarnpkg.com/) - Javascript dependency management
- [Webpack](https://webpack.js.org/) - module bundler
- [Sass](https://sass-lang.com/) - for block styles

### Usage

1. Download the zip of this repository and unzip it.
2. Navigate to the WordPress project of choice and add the unziped boilerplate in _"wp-content/plugins"_.
3. In the CMS of the project, activate the plugin. It will be shown as _"Gutenberg Custom Blocks"_.

### Getting Started

In a terminal, navigate to the root of the plugin and run `yarn` to install all needed dependencies to create custom blocks. Use `yarn watch` to watch files and recompile whenever they change.
